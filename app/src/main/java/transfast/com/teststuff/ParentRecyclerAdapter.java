package transfast.com.teststuff;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Emmanuel on 1/5/16.
 */
public class ParentRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_HOLDER_PARENT = 0;
    private static final int VIEW_HOLDER_CHILD = 1;

    private ArrayList<String> listItem;
    private Context mContext;

    public ParentRecyclerAdapter(Context context, ArrayList<String> listItem) {
        mContext = context;
        this.listItem = listItem;
    }

    @Override
    public int getItemCount() {
        return listItem.size() + 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_HOLDER_PARENT:
                return new ViewHolderParent(LayoutInflater.from(parent.getContext()).inflate(R.layout.main_recylcer_view_layout, parent, false));
            case VIEW_HOLDER_CHILD:
                return new ViewHolderChild(LayoutInflater.from(parent.getContext()).inflate(R.layout.child_recylcer_view_layout, parent, false));
            default:
                return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return VIEW_HOLDER_CHILD;
        } else {
            return VIEW_HOLDER_PARENT;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder v, int pos) {
        if (v instanceof ViewHolderParent) {
            ((ViewHolderParent) v).textView.setText(listItem.get(pos - 1));
        }
    }

    public class ViewHolderParent extends RecyclerView.ViewHolder {

        private TextView textView;

        public ViewHolderParent(final View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.main_textView);
        }
    }
    public boolean isHeader(int position) {
        return position == 0;
    }

    public class ViewHolderChild extends RecyclerView.ViewHolder {

        private RecyclerView recycler;
        private ChildRecyclerAdapter adapter;

        public ViewHolderChild(final View itemView) {
            super(itemView);
            ArrayList<String> strings = new ArrayList<>();
            strings.add("Child Value 1");
            strings.add("Child Value 2");
            strings.add("Child Value 3");
            strings.add("Child Value 1");
            strings.add("Child Value 2");
            strings.add("Child Value 3");
            strings.add("Child Value 1");
            strings.add("Child Value 2");
            strings.add("Child Value 3");
            strings.add("Child Value 1");
            strings.add("Child Value 2");
            strings.add("Child Value 3");
            recycler = (RecyclerView) itemView.findViewById(R.id.child_recycler);
            adapter = new ChildRecyclerAdapter(strings);
            recycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            recycler.setAdapter(adapter);
        }
    }
}

