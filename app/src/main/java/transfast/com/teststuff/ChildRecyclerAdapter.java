package transfast.com.teststuff;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Emmanuel on 1/5/16.
 */
public class ChildRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> listItem;

    public ChildRecyclerAdapter(ArrayList<String> listItem) {
        this.listItem = listItem;
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.child_recylcer_view_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder v, int pos) {
        ((ViewHolder) v).textView.setText(listItem.get(pos));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        public ViewHolder(final View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.child_textView);
        }
    }
}

