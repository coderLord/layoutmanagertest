package transfast.com.teststuff;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> strings = new ArrayList<>();
        strings.add("Value 1");
        strings.add("Value 2");
        strings.add("Value 3");
        strings.add("Value 4");
        strings.add("Value 5");
        strings.add("Value 6");
        strings.add("Value 7");
        strings.add("Value 8");

       final ParentRecyclerAdapter adapter = new ParentRecyclerAdapter(this, strings);
        RecyclerView recycler = (RecyclerView) findViewById(R.id.main_recycler);
      final  GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this, 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                    return adapter.isHeader(position) ? gridLayoutManager.getSpanCount() : 1;
            }
        });
        recycler.setLayoutManager(gridLayoutManager);
        recycler.setAdapter(adapter);
    }
}
